# 65Sunlight

A 65%-inspired keyboard layout, merged with Sun/unix goodness.

This is just another 65%-ish keyboard layout with function keys to the left, a layer (function) key modifier,
and capslock where it *should* be. Other oddities include ESC where tilde normally is, and a few rearranged
keys to match the old Sun UNIX layout.

Why? Because I miss my trusty Sun keyboard and haven't found a good replacement for it in a mechanical
layout. Also, I don't use my numpad at home, so why have it use up desk space?

This board will be designed using MX-style switches.
